import { DataGrid } from "@mui/x-data-grid";
import { Box } from "@mui/system";
import { Button } from "@mui/material";
import React from "react";
import { useNavigate } from "react-router-dom";

const CompanyList = ({ companyList }) => {
  const navigate = useNavigate();
  console.log(companyList);
  const columns = [
    { field: "id", headerName: "ID", flex: 0.5 },
    { field: "label", headerName: "Company Name", flex: 0.5 },
  ];

  return (
    <Box sx={{ height: 400, width: "100%" }}>
      <Button
        variant="contained"
        type="submit"
        onClick={(event) => {
          event.preventDefault();
          navigate("/");
        }}
      >
        Add Company
      </Button>
      <DataGrid
        columns={columns}
        rows={companyList}
        pageSize={5}
        rowsPerPageOptions={[5]}
      ></DataGrid>
    </Box>
  );
};

export default CompanyList;
