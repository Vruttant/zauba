import { useState } from "react";
import CompanyList from "./CompanyList";
import React from "react";
import { Routes, Route, useNavigate } from "react-router-dom";
import AddCompany from "./AddCompany";

const App = () => {
  const [allCompanies, setAllCompanies] = useState([
    { label: "Google", id: 1 },
    { label: "Facebook", id: 2 },
  ]);
  const navigate = useNavigate();
  const [selectedCompany, setSelectedCompany] = useState(null);
  const [selectedCompanies, setSelectedCompanies] = useState([]);
  const formSubmitHandler = (event) => {
    event.preventDefault();
    setSelectedCompanies([...selectedCompanies, selectedCompany]);
    setSelectedCompany(null);
    navigate("/your-companies");
  };
  return (
    <Routes>
      <Route
        path="/"
        element={
          <AddCompany
            formSubmitHandler={formSubmitHandler}
            initialCompanyList={allCompanies}
            setSelectedCompany={setSelectedCompany}
          />
        }
      ></Route>
      <Route
        path="/your-companies"
        element={<CompanyList companyList={selectedCompanies} />}
      ></Route>
    </Routes>
  );
};

export default App;
