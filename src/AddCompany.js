import { Button, Autocomplete, TextField, Typography } from "@mui/material";
const AddCompany = ({
  formSubmitHandler,
  initialCompanyList,
  setSelectedCompany,
}) => {
  return (
    <div>
      <form onSubmit={formSubmitHandler}>
        <Autocomplete
          disablePortal
          id="combo-box-demo"
          options={initialCompanyList}
          sx={{ width: 300 }}
          renderInput={(params) => <TextField {...params} label="Company" />}
          onChange={(event, value) => {
            setSelectedCompany(value);
            console.log(value);
          }}
        />
        <Button variant="contained" type="submit">
          Submit
        </Button>
      </form>
    </div>
  );
};
export default AddCompany;
